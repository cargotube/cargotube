-module(cargotube_init).
-behaviour(gen_server).


%% API.
-export([start_link/0]).
-export([stop/1]).

%% %% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {
                 config = #{}
                }).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, no_params, []).


stop(Pid) ->
    gen_server:cast(Pid, stop).

init(no_params) ->
    gen_server:cast(self(), check_config_not_started),
    {ok, #state{}}.

handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

handle_info(_Request, State) ->
    {noreply, State}.

handle_cast(check_config_not_started, State) ->
    NewState = start_if_not_started_before(State),
    {noreply, NewState};
handle_cast(log_version, State) ->
    log_version(),
    gen_server:cast(self(), read_config),
    {noreply, State};
handle_cast(read_config, State) ->
    NewState = read_config(State),
    gen_server:cast(self(), start_routing),
    {noreply, NewState};
handle_cast(start_routing, State) ->
    start_routing(State),
    gen_server:cast(self(), start_realms),
    {noreply, State};
handle_cast(start_realms, State) ->
    start_realms(State),
    gen_server:cast(self(), start_transports),
    {noreply, State};
handle_cast(start_transports, State) ->
    start_transports(State),
    gen_server:cast(self(), configure_logging),
    {noreply, State};
handle_cast(configure_logging, State) ->
    configure_logging(State),
    gen_server:cast(self(), stop),
    {noreply, State};
handle_cast(stop, #state{} = State) ->
    lager:info("stopping init"),
    {stop, normal, State};
handle_cast(_Msg, State) ->
    {noreply, State}.

log_version() ->
    Version = update_version_settings(),
    lager:info("CargoTube Version ~s", [Version]).

read_config(State)  ->
    lager:info("reading config"),
    Config = cargotube_config:read(),
    State#state{config = Config}.

start_routing(#state{config = Config}) ->
    lager:info("starting routing"),
    ct_router:init(Config).

start_realms(#state{config = Config}) ->
    Realms = maps:get(realms, Config),
    ct_auth:init(Config),
    ct_auth:start_realms(Realms).

start_transports(#state{config = Config}) ->
    Transports = maps:get(transports, Config),
    ct_gate:start_transports(Transports).


update_version_settings() ->
    {ok, Version} = application:get_key(vsn),
    application:set_env(cargotube, version, Version),
    Version.

configure_logging(#state{config = Config}) ->
    LoggingConf = maps:get(logging, Config, #{}),
    LogLevelBin = maps:get(level, LoggingConf, <<"warning">>),
    LogLevel = to_log_level(LogLevelBin),
    lager:info("setting log level to ~p", [LogLevel]),
    lager:set_loglevel(lager_file_backend, "log/cargotube.log", LogLevel).


to_log_level(Binary) ->
    try
        binary_to_existing_atom(Binary, utf8)
    catch
      _ ->
        system_halt("unknwon logging level ~p", [Binary], 3)
    end.



terminate(normal, _State) ->
    ok;
terminate(Reason, _State) ->
    system_halt("Init: terminated with reason ~p", [Reason], 2).

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

start_if_not_started_before(State) ->
    BeenStarted = application:get_env(cargotube, config_started),
    start_if_undefined(BeenStarted),
    State.

start_if_undefined(undefined) ->
    ok = application:set_env(cargotube, config_started, true),
    gen_server:cast(self(), log_version);
start_if_undefined(_) ->
    system_halt("Configure: restarted ... this should never happen!", [], 1),
    erlang:halt(1).

-ifndef(TEST).
%% @doc halt the system with a message and a number.
%% The halt is delayed for a second to ensure the logs get written.
-spec system_halt(string(), [any()], integer()) -> ok.
-dialyzer({nowarn_function, system_halt/3}).
system_halt(Message, Params, Number) ->
    lager:critical(Message, Params),
    timer:sleep(1000),
    erlang:halt(Number),
    ok.

-else.
%% @doc the system halt for testing purposes.
%% So the tests (the vm) are not halted by this function
-spec system_halt(string(), [any()], integer()) -> ok.
system_halt(Message, Params, Number) ->
    Msg = io_lib:format(Message, Params),
    io:format("would halt system with ~p due to ~s", [Number, Msg]),
    ok.

-endif.

