-module(cargotube_config).

-export([read/0]).

read() ->
    {ok, ConfigFile} = find_config_file(),
    read_config_file(ConfigFile).


find_config_file() ->
    {OsType, _} = os:type(),
    ConfigFiles = list_of_files_for(OsType),
    find_config_file(ConfigFiles, undefined).

list_of_files_for(unix) ->
     User = os:getenv("HOME"),
     System = filename:join(["/", "etc", "cargotube", "cargotube.conf"]),
     maybe_add_user_path(User, [System]);
list_of_files_for(win32) ->
    {ok, Dir} = file:get_cwd(),
    ConfigFile = filename:join([Dir, "Conf", "cargotube.conf"]),
    [ConfigFile].

maybe_add_user_path(false, FileList) ->
    FileList;
maybe_add_user_path(Home, FileList) ->
    HomePath = filename:join([Home, ".config", "cargotube", "cargotube.conf"]),
    [ HomePath | FileList].

find_config_file(_, {ok, File}) ->
    {ok, File};
find_config_file([], _) ->
    throw(no_config_found);
find_config_file([ File | List ], _) ->
    IsFile = filelib:is_regular(File),
    find_config_file(List, file_result(IsFile, File)).

file_result(true, File) ->
    {ok, File};
file_result(false, _) ->
    undefined.

read_config_file(File) ->
    {ok, Data} = file:read_file(File),
    Config = jsone:decode(Data, [{keys, attempt_atom}, {object_format, map}]),
    validate_config(Config).


validate_config(Config) ->
    lager:info("validating config"),
    MandatoryKeys = [transports, realms],
    MissingKeys = find_missing_map_keys(MandatoryKeys, [], Config),
    maybe_missing_keys_error(MissingKeys),
    ConvertSubMaps = [transports, realms],
    UpdatedConfig = convert_submaps_to_binary_keys(ConvertSubMaps, Config),
    Transports = maps:get(transports, UpdatedConfig),
    Realms = maps:get(realms, UpdatedConfig),
    ensure_used_transports_exist(Transports, Realms),
    ensure_all_transports_are_used(Transports, Realms),
    lager:info("loaded config ~p", [UpdatedConfig]),
    UpdatedConfig.


convert_submaps_to_binary_keys([], Config) ->
    Config;
convert_submaps_to_binary_keys([Key | T] , Config) ->
    Map = maps:get(Key, Config),
    UpdatedMap = convert_keys_to_binary(Map),
    UpdatedConfig = maps:put(Key, UpdatedMap, Config),
    convert_submaps_to_binary_keys(T, UpdatedConfig).


convert_keys_to_binary(Map) when is_map(Map) ->
   Keys = maps:keys(Map),
   convert_keys_to_binary(Keys, Map);
convert_keys_to_binary(NoMap) ->
   NoMap.

convert_keys_to_binary([], Map) ->
    Map;
convert_keys_to_binary([Key | T], Map) when is_binary(Key) ->
    convert_keys_to_binary(T, Map);
convert_keys_to_binary([Key | T], Map) when is_atom(Key) ->
    BinKey = atom_to_binary(Key, utf8),
    Data = maps:get(Key, Map),
    Map1 = maps:remove(Key, Map),
    UpdatedMap = maps:put(BinKey, Data, Map1),
    convert_keys_to_binary(T, UpdatedMap).


ensure_used_transports_exist(Transports, Realms) ->
    RealmNames = maps:keys(Realms),
    check_transports_exist(RealmNames, Realms, Transports).

check_transports_exist([], _Realms, _Transports) ->
    ok;
check_transports_exist([H | T], Realms, Transports) ->
    Realm = maps:get(H, Realms),
    NeededTransports = maps:get(transports, Realm, []),
    MissingTransports = find_missing_map_keys(NeededTransports, [], Transports),
    maybe_transports_error(H, NeededTransports, MissingTransports),
    check_transports_exist(T, Realms, Transports).

maybe_transports_error(RealmName, [], _ ) ->
    lager:error("realm ~p doesnt use a transport", [RealmName]),
    throw({realm_without_transport, RealmName});
maybe_transports_error(_RealmName, _, [] ) ->
    ok;
maybe_transports_error(Realm, _, Missing ) ->
    lager:error("realm ~p uses unconfigured transports: ~p", [Realm, Missing]),
    throw({realm_with_missing_transport, Realm, Missing}).


ensure_all_transports_are_used(Transports, Realms) ->
    TransportIds = maps:keys(Transports),
    RealmNames = maps:keys(Realms),
    ensure_transports_are_used(RealmNames, Realms, TransportIds).

ensure_transports_are_used(_, _, []) ->
    ok;
ensure_transports_are_used([], _Realms, TransportIds) ->
    lager:error("transports ~p are configured but never used", [TransportIds]),
    throw({unused_transports, TransportIds});
ensure_transports_are_used([RealmName | T ], Realms, TransportIds) ->
    Data = maps:get(RealmName, Realms),
    UsedTransports = maps:get(transports, Data, []),
    NewTransportIds = remove_used_transports(UsedTransports, TransportIds),
    ensure_transports_are_used(T, Realms, NewTransportIds).


remove_used_transports([], TransportIds) ->
    TransportIds;
remove_used_transports([Id | T], TransportIds) ->
    NewIds = lists:delete(Id, TransportIds),
    remove_used_transports(T, NewIds).



find_missing_map_keys([], Missing, _Map) ->
    Missing;
find_missing_map_keys([H | T], Missing, Map) ->
    Exists = maps:is_key(H, Map),
    NewMissing = add_if_missing(Exists, H, Missing),
    find_missing_map_keys(T, NewMissing, Map).


add_if_missing(true, _Entry, Missing) ->
    Missing;
add_if_missing(false, Entry, Missing) ->
    [Entry | Missing].


maybe_missing_keys_error([]) ->
    ok;
maybe_missing_keys_error(MissingKeys) ->
    lager:error("the configuration is missing the keys ~p", [MissingKeys]),
    throw({missing_config_keys, MissingKeys}).

