%%%-------------------------------------------------------------------
%% @doc cargotube top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(cargotube_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Procs = [
             config_worker()
            ],
    Flags = #{},
    {ok, { Flags, Procs } }.

%%====================================================================
%% Internal functions
%%====================================================================

config_worker() ->
    #{ id => init,
       start => {cargotube_init, start_link, []},
       restart => transient
      }.
