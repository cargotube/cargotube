;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;Include Windows Version checking

  !include "WinVer.nsh"

;--------------------------------
;Include Functions for Files 
;
  !include "FileFunc.nsh"
  
;--------------------------------
;Include CargoTube Version

  !include "CargoTubeVersion.nsh"  

;--------------------------------
;General
  !define NAME "CargoTube"
  !define COMMENT "CargoTube - The software router"
  !define ARP "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}"



  ;Name and file
  Name "${Name}"
  OutFile "${Name}_Setup-${VERSION}.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES64\${Name}"

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "..\..\LICENSE.txt"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES

  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Function .onInit
  SetRegView 64

  ; Check to see if already installed
  ReadRegStr $R0 HKLM "${ARP}" "DisplayName"
  ReadRegStr $R1 HKLM "${ARP}" "DisplayVersion"
  ReadRegStr $R2 HKLM "${ARP}" "UninstallString"
  StrCmp $R0 "" NotInstalled

  ;Ask for uninstalling before installing this version
  IfFileExists "$R2" CanBeUninstalled CannotBeUninstalled

  CanBeUninstalled:
    DetailPrint "existing installation of ${NAME} $R1 found at $R2"
    MessageBox MB_YESNO "${NAME} $R1 is already installed. Before running the installation this needs to be uninstalled. Uninstall the existing version?" /SD IDYES IDNO Quit 
    Exec $R2
    Goto Quit

  CannotBeUninstalled:
    DetailPrint "existing installation of ${NAME} $R1 found, yet no uninstaller found"
    MessageBox MB_OK "${NAME} $R1 is already installed. Please uninstall the existing version" 
    Goto Quit

  Quit:
    Quit

   NotInstalled:
FunctionEnd


Section "CargoTube" SecCargoTube
  SetOutPath "$INSTDIR"
  DetailPrint "no previous installation of ${NAME} found"

  ; remove erl.ini to not link the path of the erlang vm to the current development directory
  ; remove unneeded file types: bat, log, dump, debug, pdf, html, src
  File /r /x erl.ini /x *.bat /x *.log /x *.dump /x *.debug.* /x *.pdf /x *.html /x src\*.* ..\..\_build\default\rel\cargotube\*.*

  File /oname=bin\service_reg.bat service_reg.bat

  CreateDirectory $INSTDIR\conf
  File /oname=conf\CargoTube.conf ..\..\config\cargotube.conf 

  ExecWait '"$INSTDIR\bin\service_reg.bat" install'

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  ;Add CargotTube to "Programs and Features"
  WriteRegStr HKLM "${ARP}" "DisplayName" "${Name}"  
  WriteRegStr HKLM "${ARP}" "DisplayVersion" "${Version}"  
  WriteRegStr HKLM "${ARP}" "Publisher" "Bas Wegh"  
  WriteRegStr HKLM "${ARP}" "UninstallString" "$\"$INSTDIR\Uninstall.exe$\"" 
  WriteRegStr HKLM "${ARP}" "QuietUninstallString" "$\"$INSTDIR\Uninstall.exe$\" /S" 

  ;Calculate the size of the installation
  ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
  IntFmt $0 "0x%08X" $0
  WriteRegDWORD HKLM "${ARP}" "EstimatedSize" "$0"
  

${If} ${IsWin10}
     MessageBox MB_OK|MB_ICONEXCLAMATION "Windows 10 detected. Please change the service to start 'automatically (delayed)'"
${EndIf}

SectionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"
  SetRegView 64
  ;Remove CargotTube from "Programs and Features"
  DeleteRegKey HKLM "${ARP}" 

  ExecWait '"$INSTDIR\bin\service_reg.bat" remove'
  ; ensure epmd is stopped
  ExecWait 'taskkill /F /IM epmd.exe'
  Delete "$INSTDIR\Uninstall.exe"

  RMDir /r "$INSTDIR"

SectionEnd
